package com.dejamobile.cloudbasedpayment.app.api;

public class TokenResponse {
    String type;
    String token;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
