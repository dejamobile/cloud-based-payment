package com.dejamobile.cloudbasedpayment.app.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="TokenServiceProvider", url="http://localhost:3000/card")
public interface TokenServiceProviderClient {
    @RequestMapping(method = RequestMethod.GET, value = "/visa")
    TokenResponse getVisaToken(@RequestParam String cardNumber);

    @RequestMapping(method = RequestMethod.GET, value = "/mastercard")
    TokenResponse getMastercardToken(@RequestParam String cardNumber);
}
