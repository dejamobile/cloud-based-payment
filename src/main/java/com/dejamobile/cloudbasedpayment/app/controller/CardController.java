package com.dejamobile.cloudbasedpayment.app.controller;

import com.dejamobile.cloudbasedpayment.app.api.TokenResponse;
import com.dejamobile.cloudbasedpayment.app.api.TokenServiceProviderClient;
import com.dejamobile.cloudbasedpayment.app.response.DigitalCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("cards")
public class CardController {
    @Autowired
    public TokenServiceProviderClient tokenServiceProviderClient;

    @GetMapping(produces = "application/json")
    public DigitalCard getDigitalCard(@RequestParam String cardNumber, @RequestParam String cardType) {

        DigitalCard digitalCard = new DigitalCard();
        digitalCard.setCardType(cardType);
        if("visa".equals(cardType)){
            TokenResponse tokenResponse = tokenServiceProviderClient.getVisaToken(cardNumber);
            digitalCard.setDigitalCardNumber(tokenResponse.getToken());
        }
        else {
            TokenResponse tokenResponse = tokenServiceProviderClient.getMastercardToken(cardNumber);
            digitalCard.setDigitalCardNumber(tokenResponse.getToken());
        }
        return digitalCard;
    }
}
