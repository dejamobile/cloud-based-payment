package com.dejamobile.cloudbasedpayment.app.response;

public class DigitalCard {
    private String digitalCardNumber;
    private String cardType;

    public String getDigitalCardNumber() {
        return digitalCardNumber;
    }

    public void setDigitalCardNumber(String digitalCardNumber) {
        this.digitalCardNumber = digitalCardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}
